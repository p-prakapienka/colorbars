package by.restrictor.colorprogressbars;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import java.util.concurrent.Executors;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProgressFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProgressFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProgressFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private ProgressBar mProgressBar;
    private ImageButton mProgressButton;

    private AsyncTask progressTask;

    public ProgressFragment() {
        // Required empty public constructor
    }

    public static ProgressFragment newInstance() {
        return new ProgressFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.progress_fragment, null);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mProgressBar.setProgressTintList(ColorStateList.valueOf(Color.RED));
        mProgressButton = (ImageButton) view.findViewById(R.id.progressButton);
        mProgressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(ProgressFragment.this);
                if (progressTask != null) {
                    progressTask.cancel(true);
                }
            }
        });
        progressTask = new ProgressTask().executeOnExecutor(Executors.newSingleThreadExecutor());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Fragment fragment);
    }

    private class ProgressTask extends AsyncTask<Void, Void, Void> {

        private int progress;

        @Override
        protected Void doInBackground(Void... params) {
            Log.d("Fragment", "Task started");
            while (progress != 100) {
                publishProgress();
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {

                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            mProgressBar.incrementProgressBy(1);
            progress = mProgressBar.getProgress();
            if (progress == 33) {
                mProgressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#ffff8800")));
            } else if (progress == 66) {
                mProgressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#ff99cc00")));
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mProgressButton.setBackgroundResource(R.mipmap.ic_done_black_24dp);
            mProgressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#ff99cc00")));
        }
    }
}
